﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MangerForScanner
{
    public delegate void UserControlSetDelegate(UserControlEventArgs uc);
    public partial class WelcommingPage : UserControl
    {
        public UserControlSetDelegate userControlSetEvent;

        public WelcommingPage()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            UserControlEventArgs uc = new UserControlEventArgs(new ScannerInitialize());
            if (userControlSetEvent != null)
                userControlSetEvent(uc);
             
        }

    }
}
