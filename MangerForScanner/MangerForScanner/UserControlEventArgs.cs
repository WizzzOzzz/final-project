﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MangerForScanner
{
    public class UserControlEventArgs : EventArgs
    {
        private UserControl uc;  
        public UserControlEventArgs(UserControl uc)
        {
            this.uc = uc;
        }
        public UserControl userControl
        {
            get
            {
                return uc;
            }
        }
    }
}
