﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Emgu.CV;
using Emgu.Util;
using Emgu.CV.OCR;
using Emgu.CV.Structure;
using Emgu.CV.UI;
using System.Net.Mail;




namespace OCR_PROJECT
{
    public partial class Form1 : Form
    {
        Tesseract OCRz = new Tesseract("tessdata", "eng", Tesseract.OcrEngineMode.OEM_TESSERACT_ONLY);
        static Bitmap pic;
        ImageViewer viewer = new ImageViewer(); //create an image viewer

        String google_path = "https://www.google.co.il/#q=";
        private String mail_address;

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

       

        private void button1_Click(object sender, EventArgs e)
        {
           PictureBox1.Image = viewer.Image.Bitmap;


            //grayscale();
            //medianFilter(5);
            black_and_white();
            reduce_noise(6,6);

            pic = (Bitmap)PictureBox1.Image;
            var testImagePath=PictureBox1.Image;
            Image<Bgr, Byte> myImage = new Image<Bgr, Byte>(pic);             
            OCRz.Recognize(myImage);
            RichTextBox1.Text = OCRz.GetText();

        }


        public void reduce_noise(int f_width, int f_height)
        {
            Bitmap bt = (Bitmap)PictureBox1.Image;
            int[,] picture = new int[bt.Width, bt.Height];



            for (int i = 0; i < bt.Width; i++)
            {
                for (int j = 0; j < bt.Height; j++)
                {
                    picture[i, j] = bt.GetPixel(i, j).R;
                }
            }

            
            int width = f_width;
            int height = f_height;
            int z = 1;
            int[] flag = new int[z];

            int[,] tmpM = new int[width, height];
           



            int pixelVal = 0;

            for (int i = (width / 2) + 1; i < bt.Width - width + width / 2 - 1; i++)
            {

                for (int j = (height / 2) + 1; j < bt.Height - height + height / 2 - 1; j++)
                {
                    for (int p = -1; p < width - 1; p++)
                    {

                        for (int q = -1; q < height - 1; q++)
                        {
                            if ((i + p) < bt.Width && (q + 1) < bt.Height)
                                tmpM[p + 1, q + 1] = picture[i + p, j + q];
                            else
                                tmpM[p + 1, q + 1] = 255;


                        }
                    }



                    
                    
                    pixelVal = 0;

                    for (int n = 0; n < 3; n++)
                    {
                        for (int m = 0; m < 3; m++)
                        {
                            if (tmpM[n, m] == 255)
                                pixelVal = 255;
                        }
                    }






                    Color col = Color.FromArgb(pixelVal, pixelVal, pixelVal);
                    bt.SetPixel(i, j, col);

                }
            }


            PictureBox1.Image = bt;

        }


        private void button2_Click(object sender, EventArgs e)
        {
           //ImageViewer viewer = new ImageViewer(); //create an image viewer
           Capture capture = new Capture(); //create a camera captue
          
            Application.Idle += new EventHandler(delegate(object sender2, EventArgs e2)
            {  //run this until application closed (close button click on image viewer)
                viewer.Image = capture.QueryFrame(); //draw the image obtained from camera
            });
             
            viewer.Show(); //show the image viewer
            
        }

        private void button3_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start(google_path + RichTextBox1.Text);
        }

        public void send_mail()
        {
             try
            {
                mail_address = RichTextBox1.Text.Replace('\r', ' ').Replace('\n', ' ');
        
                MailMessage mail = new MailMessage();
                SmtpClient SmtpServer = new SmtpClient("smtp.gmail.com");

                mail.From = new MailAddress("finalprojectocr@gmail.com");
                mail.To.Add(mail_address);
                mail.Subject = "Test Mail";
                mail.Body = "Image proc test";

                SmtpServer.Port = 587;
                SmtpServer.Credentials = new System.Net.NetworkCredential("finalprojectocr", "1a2s3d4f5G");
                SmtpServer.EnableSsl = true;

                SmtpServer.Send(mail);
                MessageBox.Show("mail sent");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        public void grayscale()
        {
            {
                //    dis();
                Bitmap bt = (Bitmap)PictureBox1.Image;

               
                for (int i = 0; i < bt.Width; i++)
                {
                    for (int x = 0; x < bt.Height; x++)
                    {
                        Color oc = bt.GetPixel(i, x);
                        int grayScale = (int)((oc.R * 0.3) + (oc.G * 0.59) + (oc.B * 0.11));
                        Color nc = Color.FromArgb(oc.A, grayScale, grayScale, grayScale);
                        bt.SetPixel(i, x, nc);
                    }
                }

                PictureBox1.Image = bt;
            }
        }


        public void medianFilter(int Size)
        {
            
        Bitmap Image = (Bitmap)PictureBox1.Image;

        System.Drawing.Bitmap TempBitmap = Image;
        System.Drawing.Bitmap NewBitmap = new System.Drawing.Bitmap(TempBitmap.Width, TempBitmap.Height);
        System.Drawing.Graphics NewGraphics = System.Drawing.Graphics.FromImage(NewBitmap);
        NewGraphics.DrawImage(TempBitmap, new System.Drawing.Rectangle(0, 0, TempBitmap.Width, TempBitmap.Height), new System.Drawing.Rectangle(0, 0, TempBitmap.Width, TempBitmap.Height), System.Drawing.GraphicsUnit.Pixel);
        NewGraphics.Dispose();
        Random TempRandom = new Random();
        int ApetureMin = -(Size / 2);
       int ApetureMax = (Size / 2);
       for (int x = 0; x < NewBitmap.Width; ++x)
       {
           for (int y = 0; y < NewBitmap.Height; ++y)
           {
               List<int> RValues = new List<int>();
               List<int> GValues = new List<int>();
               List<int> BValues = new List<int>();
               for (int x2 = ApetureMin; x2 < ApetureMax; ++x2)
               {
                   int TempX = x + x2;
                   if (TempX >= 0 && TempX < NewBitmap.Width)
                   {
                       for (int y2 = ApetureMin; y2 < ApetureMax; ++y2)
                       {
                           int TempY = y + y2;
                           if (TempY >= 0 && TempY < NewBitmap.Height)
                           {
                               Color TempColor = TempBitmap.GetPixel(TempX, TempY);
                               RValues.Add(TempColor.R);
                               GValues.Add(TempColor.G);
                               BValues.Add(TempColor.B);
                           }
                       }
                   }
               }
               RValues.Sort();
               GValues.Sort();
               BValues.Sort();
               Color MedianPixel = Color.FromArgb(RValues[RValues.Count / 2],
                   GValues[GValues.Count / 2], 
                   BValues[BValues.Count / 2]);
               NewBitmap.SetPixel(x, y, MedianPixel);
           }
      
         }
            PictureBox1.Image = Image;
        
    }

        public void black_and_white()
        {

            int mid = 114;
            int colR; int colG; int colB;

            Bitmap bt = new Bitmap(PictureBox1.Image);


            Color color;

            for (int i = 0; i < bt.Width; i++)
                for (int j = 0; j < bt.Height; j++)
                {
                    colR = bt.GetPixel(i, j).R;
                    if (colR > mid)
                        colR = 1;
                    else
                        colR = 0;

                    colG = bt.GetPixel(i, j).G;
                    if (colG > mid)
                        colG = 1;
                    else
                        colG = 0;

                    colB = bt.GetPixel(i, j).G;
                    if (colB > mid)
                        colB = 1;
                    else
                        colB = 0;

                    if ((colR + colG + colB) > 1)
                        color = Color.FromArgb(255, 255, 255);
                    else
                        color = Color.FromArgb(0, 0, 0);


                    bt.SetPixel(i, j, color);

                }

            PictureBox1.Image = bt;

        }

        private void button4_Click(object sender, EventArgs e)
        {
            send_mail();
        }
    }
}






